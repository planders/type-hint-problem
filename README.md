type-hint-problem
=================

This small project aims to demonstrate a problem I'm having with PyCharm
type hints where it gives apparently spurious type warnings until the file
is scanned a second time.

The problem seems to be fixed by adding `-> None` return annotations to the
`__init__` methods. My question is whether this is considered expected 
behavior specified by the type hinting system. 


Bug Reproduction
================

* Open this project with PyCharm, I'm using 2017.2 on Windows 10
* Make sure a project interpreter is selected, I'm using CPython 3.6.2
* Open the file type_hint_problem/frobnicate.py
* In PyCharm do File -> Invalidate Caches and Restart
* When file reopens, type errors are flagged at lines 28 and 37.
* Press Ctrl-Alt-O (optimize imports) or another command that rescans the file,
  or just edit some text.
* The type errors disappear.
* Problem disappears when `-> None` return annotations are added
  * ... are these required? Isn't `__init__` assumed to return None?
  
### Expected Result
 
Type hint errors are not displayed, including immediately after
rebuilding caches.
  
### Actual Result

Type hint errors are shown on lines 28 and 37 until file is scanned
a second time, then they disappear.

### Impact

In a large project this can cause a long chain of type warnings across
different modules after PyCharm caches have been cleared.

This may be fixed by updating the annotations to consistently include
`-> None` return annotations but this seems like a workaround. Or the
type hint can be set on parameters to `__init__`, but in some cases
the values I wish to hint aren't passed in as parameters. 
  
Additional remarks can be found in the file:

type_hint_problem/frobnicate.py

Comments to: planders at gmail.
