#!/usr/bin/env python

from type_hint_problem.frobnicate import factory, Blooper


# noinspection PyMethodMayBeStatic
class Client:
    def run(self):
        frob = factory()
        print(frob)
        return

    def bloop(self):
        frob = factory()
        b = Blooper(frob)
        print(b.bloop(12))

client = Client()
client.run()
client.bloop()
