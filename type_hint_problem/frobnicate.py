#!/usr/bin/env python

"""
When I load this project in PyCharm 2017.2 (Windows 10 x64), after running
"Invalidate Caches and Restart', I see apparently spurious type errors on the
lines (31 and 40) in the `__init__`s with type hints. You then run "Optimize Imports",
or other commands that rescan the files, and the errors disappear.

They say something like:

`Expected to return 'Blooper', got no return`

I am seeing this in my own project with much more complex files where the type
errors cause a chain of warnings to appear throughout my files. It seems the
problem can be fixed by adding `-> None` return annotations to the inits.
I'm not sure if this is considered expected behavior not.
Is the -> None considered required?

It can also be 'fixed' in this example by setting the type hints on the *parameters*
to init instead of inside the body, but this this a contrived example. In my real app,
the values I wish to hint aren't always passed in as parameters. """

import secrets
from type_hint_problem import FOO


class Frobber(object):
    # def __init__(self, num: int, bloop=None) -> None:  # fixes type hint error?
    def __init__(self, num: int, bloop=None):
        self.num = num
        self.bloop: 'Blooper' = bloop

    def __str__(self):
        return self.bloop.bloop(self.num)


class Blooper(object):
    # def __init__(self, frob=None) -> None:  # fixes type hint error...?
    def __init__(self, frob=None):
        self.frob: 'Frobber' = frob

    def bloop(self, num=None):
        return f"Bloop! {FOO} {num} {self.frob.num}"


def factory() -> Frobber:
    b = Blooper()
    f = Frobber(secrets.randbits(8), bloop=b)
    b.frob = f
    return f
