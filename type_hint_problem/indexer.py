#!/usr/bin/env python
# -*- coding: utf-8; mode: python; py-indent-offset: 4; indent-tabs-mode: nil -*-
# vim: fileencoding=utf-8 tabstop=4 expandtab shiftwidth=4

"""Demonstrate how PyCharm produces type warnings when indexing a sequence with an instance of a
class that implements `__index__`. An invalid warning is produced on line 33. """


class Indexer:
    def __init__(self, name: str, pos: int):
        super().__init__()
        self.name = name
        self.pos = pos

    def __str__(self):
        return "<Indexer %s:%s>" % (self.name, self.pos)

    def __int__(self) -> int:
        return self.pos

    def __index__(self) -> int:
        return self.pos


def test_indexer():
    test_list = ['spam', 'eggs', 'chorizo']
    ix_second = Indexer("second", 1)

    # Following line has warning:
    # Unexpected type: (Indexer)
    # Possible types: (int) (slice)

    result = test_list[ix_second]
    print("%s: %s" % (ix_second, result))
    assert result == "eggs", "Indexer isn't working?"
    return


if __name__ == '__main__':
    test_indexer()
